<?php

namespace Drupal\gdpr_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DownloadFileController.
 *
 * @package Drupal\gdpr_user\Controller
 */
class DownloadFileController extends ControllerBase {

  /***
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')
    );
  }



  public function download(User $user, $file_name) {

    $uri = 'private://sent-data/' . $file_name;
    $path = $this->fileSystem->realpath('private://sent-data');

    if (file_exists($uri)) {
      // Let other modules provide headers and controls access to the file.
      $headers = $this->moduleHandler()->invokeAll('file_download', [$uri]);

      $headers['Content-Type'] = isset($headers['Content-Type']) ? $headers['Content-Type'] : 'application/x-download';
      $headers['Content-Disposition'] = isset($headers['Content-Disposition']) ? $headers['Content-Disposition'] : "attachment; filename=\"{$user->getUsername()}-data.zip\"";
      $headers['Cache-Control'] = isset($headers['Cache-Control']) ? $headers['Cache-Control'] :'private, max-age=0, must-revalidate';
      $headers['Content-Length'] = isset($headers['Content-Length']) ? $headers['Content-Length'] : filesize($path . '/' . $file_name);
      $headers['Pragma'] = isset($headers['Pragma']) ? $headers['Pragma'] : 'public';

      foreach ($headers as $result) {
        if ($result == -1) {
          throw new AccessDeniedHttpException();
        }
      }

      if (count($headers)) {
        // \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onRespond()
        // sets response as not cacheable if the Cache-Control header is not
        // already modified. We pass in FALSE for non-private schemes for the
        // $public parameter to make sure we don't change the headers.
        return new BinaryFileResponse($uri, 200, $headers, FALSE);
      }

      throw new AccessDeniedHttpException();
    }

    throw new NotFoundHttpException();
  }

}
