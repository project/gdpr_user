<?php

namespace Drupal\gdpr_user\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\gdpr_user\Services\GdprUserDataServiceInterface;
use Drupal\user\Entity\User;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DownloadUserDataForm.
 *
 * @package Drupal\gdpr_user\Form
 */
class DownloadUserDataForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\File\FileSystemInterface definition
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\gdpr_user\Services\GdprUserDataServiceInterface definition.
   *
   * @var \Drupal\gdpr_user\Services\GdprUserDataServiceInterface
   */
  private $gdprUserDataService;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger, FileSystemInterface $fileSystem, GdprUserDataServiceInterface $gdprUserDataService, EntityTypeManagerInterface $entityTypeManager) {
    $this->messenger = $messenger;
    $this->fileSystem = $fileSystem;
    $this->gdprUserDataService = $gdprUserDataService;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('file_system'),
      $container->get('gdpr_user.user_data'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gdpr_download_user_data_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, User $user = NULL) {
    if (!$user instanceof User) {
      $this->messenger->addMessage($this->t('User not found.'), 'error');

      return [];
    }

    $config = $this->config('gdpr_user.settings');

    $form_state->set('gdpr_user', $user->id());

    $form['#title'] = $config->get('title');

    $form['body'] = [
      '#type' => 'processed_text',
      '#text' => isset($config->get('body')['value']) ? $config->get('body')['value'] : '',
      '#format' => isset($config->get('body')['format']) ? $config->get('body')['format'] : 'full_html',
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['prepare'] = [
      '#type' => 'submit',
      '#value' => $config->get('prepare_label'),
      '#attributes' => ['class' => ['button--primary', 'btn-primary']],
    ];

    $path = $this->fileSystem
      ->realpath('private://sent-data');
    if (!file_exists($path)) {
      $this->fileSystem->mkdir($path);
    }
    $user_files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($path),
      RecursiveIteratorIterator::LEAVES_ONLY
    );

    // Prepare download link.
    foreach ($user_files as $name => $file) {
      if (!$file->isDir()) {
        if (explode('--', $file->getFilename())[0] == 'user-' . $user->id()) {
          $form['actions']['download'] = [
            '#type' => 'link',
            '#title' => $config->get('download_label'),
            '#url' => Url::fromRoute('gdpr_users.download_file', [
              'user' => $user->id(),
              'file_name' => $file->getFilename(),
            ]),
          ];
          continue;
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = $form_state->get('gdpr_user');
    try {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->entityTypeManager
        ->getStorage('user')
        ->load($uid);

      $this->gdprUserDataService->prepareFiles($user);
    }
    catch (InvalidPluginDefinitionException $e) {
      throw new InvalidPluginDefinitionException($e);
    }
    catch (PluginNotFoundException $e) {
      throw new PluginNotFoundException($e);
    }

  }

}
