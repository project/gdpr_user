<?php

namespace Drupal\gdpr_user\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GdprUserSettingsForm.
 *
 * @package Drupal\gdpr_user\Form
 */
class GdprUserSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gdpr_user.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gdpr_user_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('gdpr_user.settings');
    $form = parent::buildForm($form, $form_state);

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Download data page title.'),
      '#required' => TRUE,
      '#default_value' => $config->get('title') ?? $this->t('Download user data'),
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Text to be displayed in the download page.'),
      '#required' => TRUE,
      '#format' => isset($config->get('body')['format']) ? $config->get('body')['format'] : 'full_html',
      '#default_value' => isset($config->get('body')['value']) ? $config->get('body')['value'] : $this->t('<p>Download all of my data.</p>
<p>As part of GDPR (The General Data Protection Regulation) every user on the site has the right to request the personal data and content on our platform. It is also your right to request that your data deleted if you choose to cancel your user account. Please note that this will not include comments as that data lives in a third party system.)</p>
<p>To download your data select "Prepare". When the data has been packaged a "Download" button will appear. Select "Download" to receive your data. Images will be provided in jpeg/png/gif format, text will be provided in a Json format. (Json is an open-standard, minimal, readable format for structuring data.)</p>'),
    ];

    $form['prepare_label'] =[
      '#type' => 'textfield',
      '#title' => $this->t('Label: Prepare'),
      '#description' => $this->t('The "Prepare" button label that the user needs to press in order to prepare the data to be downloaded.'),
      '#required' => TRUE,
      '#default_value' => $config->get('prepare_label') ?? $this->t('Prepare'),
    ];

    $form['download_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label: Download'),
      '#description' => $this->t('The "Download" button label that the user needs to press in order to download it\'s data.'),
      '#required' => TRUE,
      '#default_value' => $config->get('download_label') ?? $this->t('Download'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('gdpr_user.settings')
      ->set('title', $form_state->getValue('title'))
      ->set('body', $form_state->getValue('body'))
      ->set('prepare_label', $form_state->getValue('prepare_label'))
      ->set('download_label', $form_state->getValue('download_label'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
