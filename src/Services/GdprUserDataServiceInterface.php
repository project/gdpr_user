<?php

namespace Drupal\gdpr_user\Services;

use Drupal\user\UserInterface;

/**
 * Interface GdprUserDataServiceInterface.
 */
interface GdprUserDataServiceInterface {

  /**
   * Prepare the user sent data.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function prepareFiles(UserInterface $user);

  /**
   * Prepare the Json data.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to prepare the data.
   *
   * @return bool|string
   *   False if no data or the Json representation of all data sent by the user.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function prepareJsonData(UserInterface $user);

}
