<?php

namespace Drupal\gdpr_user\Services;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Serializer\Serializer;
use ZipArchive;

/**
 * Class GdprUserDataService.
 */
class GdprUserDataService implements GdprUserDataServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Drupal\Core\StreamWrapper\StreamWrapperManagerInterface definition.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  private $streamWrapperManager;

  /**
   * Drupal\Component\Uuid\UuidInterface definition.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  private $uuid;

  /**
   * Symfony\Component\Serializer\Serializer definition.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  private $serializer;

  /**
   * Constructs a new GdprUserDataService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   * @param \Symfony\Component\Serializer\Serializer $serializer
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, FileSystemInterface $fileSystem, StreamWrapperManagerInterface $streamWrapperManager, UuidInterface $uuid, Serializer $serializer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->fileSystem = $fileSystem;
    $this->streamWrapperManager = $streamWrapperManager;
    $this->uuid = $uuid;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFiles(User $user) {
    // Delete previous temporary files.
    $path = $this->fileSystem->realpath('private://sent-data');
    $user_files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($path),
      RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($user_files as $file) {
      if (!$file->isDir()) {
        if (explode('--', $file->getFilename())[0] == 'user-' . $user->id()) {
          $this->fileSystem->deleteRecursive($file->getPathname());
        }
      }
    }

    // Create the temporary folder.
    $dir = 'private://sent-data/user-' . $user->id();
    if (!$this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY)) {
      $this->fileSystem->mkdir($dir, 0775, TRUE);
    }

    $json = $this->prepareJsonData($user);
    if ($json) {
      $this->fileSystem->saveData($json, "$dir/data.json");
    }

    // Prepare files.
    $files_ids = $this->entityTypeManager
      ->getStorage('file')
      ->getQuery('AND')
      ->condition('uid', $user->id())
      ->execute();

    if (count($files_ids)) {
      $files = $this->entityTypeManager
        ->getStorage('file')
        ->loadMultiple($files_ids);
      /** @var \Drupal\file\Entity\File $file */
      foreach ($files as $file) {
        $uri = $file->getFileUri();
        $stream_wrapper_manager = $this->streamWrapperManager->getViaUri($uri);
        if ($stream_wrapper_manager) {
          $file_path = $stream_wrapper_manager->realpath();
          if (file_exists($file_path)) {
            $this->fileSystem->copy($file->getFileUri(), $dir . '/' . $file->getFilename(), FileSystemInterface::EXISTS_REPLACE);
          }
        }
      }
    }

    // Zip user folder.
    $uuid = $this->uuid->generate();
    $path = $this->fileSystem->realpath($dir);
    $file_name = $path . '--' . $uuid . '.zip';

    $zip = new ZipArchive();
    $zip->open($file_name, ZipArchive::CREATE);
    $user_files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($path),
      RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($user_files as $file) {
      if (!$file->isDir()) {
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($path) + 1);
        $zip->addFile($filePath, $relativePath);
      }
    }

    $zip->close();

    // Delete temporary folder.
    $this->fileSystem->deleteRecursive($dir);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareJsonData(User $user) {
    $nodes_ids = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery('AND')
      ->condition('uid', $user->id())
      ->execute();

    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadMultiple($nodes_ids);

    $json = [];
    // Add account data.
    $user_fields = $user->getFields();
    $user_data = [];
    foreach ($user_fields as $name => $field) {
      if (substr($name, 0, 5) == 'field' || in_array($name, [
          'name',
          'user_picture',
        ])) {
        $user_data[$name] = $field;
      }
    }
    $data = $this->serializer->serialize($user_data, 'json', ['plugin_id' => 'field']);
    $json[] = $this->serializer->decode($data, 'json');

    // Add nodes data.
    foreach ($nodes as $node) {
      $data = $this->serializer->serialize($node, 'json', ['plugin_id' => 'entity']);
      $json[] = $this->serializer->decode($data, 'json');

      // Include the node revisions.
      $revision_ids = $this->entityTypeManager->getStorage('node')
        ->revisionIds($node);

      if (count($revision_ids)) {
        foreach ($revision_ids as $vid) {
          $revision = $this->entityTypeManager->getStorage('node')
            ->loadRevision($vid);
          if ($revision->getLoadedRevisionId() != $node->getLoadedRevisionId()) {
            $data = $this->serializer->serialize($revision, 'json', ['plugin_id' => 'entity']);
            $json[] = $this->serializer->decode($data, 'json');
          }
        }
      }
    }

    return $this->serializer->serialize($json, 'json', ['plugin_id' => 'entity']);
  }

}
