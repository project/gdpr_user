<?php

namespace Drupal\gdpr_user\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\Route;

/**
 * Class GdprUserAccessCheck.
 *
 * @package Drupal\gdpr_user\Access
 */
class GdprUserAccessCheck implements ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * Checks access.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\user\Entity\User $user
   *   The user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, AccountInterface $account, User $user = NULL) {
    $permission = $route->getRequirement('_permission');

    $own_account_permissions = [
      'download user own sent data',
      'request own account and data deletion',
    ];

    $all_account_permissions = [
      'download all user sent data',
      'request all account and data deletion',
    ];

    $user = $this->container->get('current_route_match')->getParameter('user');
    if (!$user instanceof User && is_numeric($user)) {
      $user = User::load($user);
      if (!$user instanceof User) {
        return AccessResult::forbidden();
      }
    }

    if ($account->id() == $user->id()) {
      $split = explode(',', $permission);
      if (count($split) > 1) {
        return AccessResult::allowedIfHasPermissions($account, array_intersect($own_account_permissions, $split), 'AND');
      }
      else {
        $split = explode('+', $permission);
        return AccessResult::allowedIfHasPermissions($account, array_intersect($own_account_permissions, $split), 'AND');
      }
    }
    else {
      $split = explode(',', $permission);
      if (count($split) > 1) {
        return AccessResult::allowedIfHasPermissions($account, array_intersect($all_account_permissions, $split), 'AND');
      }
      else {
        $split = explode('+', $permission);
        return AccessResult::allowedIfHasPermissions($account, array_intersect($all_account_permissions, $split), 'AND');
      }
    }

  }

}
